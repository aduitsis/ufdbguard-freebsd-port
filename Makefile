# Created by: Athanasios Douitsis <aduitsis@cpan.org>
# $FreeBSD$

PORTNAME=		ufdbguard
PORTVERSION=		${MAJOR}.${MINOR}
PORTREVISION?=		0
CATEGORIES=		www
MASTER_SITES=		${MASTER_SITE_SOURCEFORGE}
MASTER_SITE_SUBDIR=	${PORTNAME}/${MAJOR}
DISTNAME=		${UFDBGUARD}-${DISTVERSIONPREFIX}${MAJOR}-${MINOR}${DISTVERSIONSUFFIX}

MAINTAINER=		aduitsis@cpan.org
COMMENT=		UfdbGuard, a URL filter for Squid

LICENSE=		GPLv2

RUN_DEPENDS=		wget:${PORTSDIR}/ftp/wget \
			squid:${PORTSDIR}/www/squid

GNU_CONFIGURE=		yes
WRKSRC=			${WRKDIR}/${UFDBGUARD}-${MAJOR}
CONFIGURE_ARGS+=	--with-ufdb-user=${USER} \
			--with-ufdb-images_dir=${DATADIR}/images \
			--with-ufdb-samplesdir=${DATADIR}/samples \
			--with-ufdb-mandir=${PREFIX}/man \
			--with-ufdb-dbhome=${ETCDIR}/blacklists \
			--with-ufdb-logdir=/var/log \
			--with-ufdb-piddir=/var/run \
			--with-ufdb-config=${ETCDIR}

USE_RC_SUBR=		ufdbguard

SUB_FILES=		pkg-message
SUB_LIST+=		USER=${USER}

UFDBGUARD=		ufdbGuard
MAJOR=			1.31
MINOR=			16
USER=			squid

post-stage:
	${MV} ${STAGEDIR}${PREFIX}/etc/rc.d/ufdb.sh ${STAGEDIR}${ETCDIR}/ufdb.sh
	${MV} ${STAGEDIR}${ETCDIR}/ufdbGuard.conf ${STAGEDIR}${ETCDIR}/ufdbGuard.conf.sample

.include <bsd.port.mk>
